﻿using AutoMapper.Internal;
using Core.Interfaces.SPI;
using Core.Models;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace Infrastructure.Services
{
    public class RoleProvider : IRoleProvider
    {
        private readonly AuthenAndAuthorContext _dbContext;

        public RoleProvider(AuthenAndAuthorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddNewRole(Role role)
        {
            try
            {
                bool isAnyUserHasTheSameCode = await _dbContext.Roles.AnyAsync(x => x.Code == role.Code);

                if (isAnyUserHasTheSameCode)
                {
                    throw new ArgumentException("The code already exist");
                }

                _dbContext.Add(role);
                _dbContext.SaveChanges(); 
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
           
        }
    }
}