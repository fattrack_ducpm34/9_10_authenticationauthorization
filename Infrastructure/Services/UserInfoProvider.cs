﻿using Core.Interfaces.SPI;
using Core.Models;
using Infrastructure.Helpers;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services
{
    public class UserInfoProvider : IUserInfoProvider
    {
        private readonly AuthenAndAuthorContext _dbContext;

        public UserInfoProvider(AuthenAndAuthorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task ChangePassword(int id, string oldPassword, string newPassword)
        {
            try
            {
                User userToUpdate = await _dbContext.Users.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (userToUpdate == null)
                {
                    throw new ArgumentException("User does not exist");
                }

                var passMessage = PasswordHelper.CheckPasswordStrength(newPassword);
                if (!string.IsNullOrEmpty(passMessage))
                {
                    throw new ArgumentException(passMessage);
                }

                if (!PasswordHelper.VerifyPassword(oldPassword, userToUpdate.Password))
                {
                    throw new ArgumentException("Password is Incorrect");
                }

                userToUpdate.Password = PasswordHelper.HashPassword(newPassword);

                _dbContext.Update(userToUpdate);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
        }

        public async Task UpdateUserFullName(int id, string fullName)
        {
            try
            {
                var userToUpdate = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == id);

                if (userToUpdate == null)
                {
                    throw new ArgumentException("User does not exist");
                }
                userToUpdate.Fullname = fullName;

                _dbContext.Update(userToUpdate);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
        }
    }
}