﻿using Core.Interfaces.SPI;
using Core.Models;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services
{
    public class UserRoleProvider : IUserRoleProvider
    {
        private readonly AuthenAndAuthorContext _dbContext;

        public UserRoleProvider(AuthenAndAuthorContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddRoleForUser(int userId, int roleId)
        {
            try
            {
                var isAnyUserRoleExisted = await _dbContext.UserRoles.AnyAsync(x => x.UserId == userId && x.RoleId == roleId);
                if (isAnyUserRoleExisted)
                {
                    throw new ArgumentException("User and Role already exist in UserRole tablel");
                }

                var isUserExist = await _dbContext.Users.AnyAsync(x => x.Id == userId);
                var isRoleExist = await _dbContext.Roles.AnyAsync(x => x.Id == roleId);
                if (!isUserExist || !isRoleExist)
                {
                    throw new ArgumentException("UserId or RoleId does not exist. Plese check");
                }

                UserRole userRole = new UserRole
                {
                    UserId = userId,
                    RoleId = roleId
                };

                _dbContext.Add(userRole);
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
        }

        public async Task<List<UserRole>> GetUserRolesByUserId(int userId)
        {
            var userRoles = await _dbContext.UserRoles
                .Where(x => x.UserId == userId)
                .Include(ur => ur.Role)
                .Include(ur => ur.User)
                .ToListAsync();

            return userRoles;
        }

        public async Task RemoveRoleOfUser(int userId, int roleId)
        {
            try
            {
                var userRoleToDelete = await _dbContext.UserRoles.Where(x => x.UserId == userId && x.RoleId == roleId).FirstOrDefaultAsync();
                if (userRoleToDelete == null)
                {
                    throw new ArgumentException("User and Role does not exist");
                }

                _dbContext.Remove(userRoleToDelete);
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw new SystemException($"Something wrong happen with database. Detail: {e.Message}");
            }
        }
    }
}