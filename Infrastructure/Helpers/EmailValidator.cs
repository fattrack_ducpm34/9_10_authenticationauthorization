﻿using System.Text.RegularExpressions;

namespace Infrastructure.Helpers
{
    public class EmailValidator
    {
        private static readonly Regex EmailRegex = new Regex(@"^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$");

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            email = email.Trim();

            if (email.Length > 256)
                return false;

            return EmailRegex.IsMatch(email);
        }
    }
}