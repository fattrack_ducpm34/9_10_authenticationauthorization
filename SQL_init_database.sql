CREATE DATABASE AUTHEN_AUTHOR

USE AUTHEN_AUTHOR

GO

-- Create User table
CREATE TABLE "User" (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    Username NVARCHAR(100),
    Fullname NVARCHAR(100),
    Email NVARCHAR(100),
	"Password" NVARCHAR(100),
	Token NVARCHAR(MAX),
	RefreshToken NVARCHAR(MAX),
	RefreshTokenExpiryTime DATETIME
);

-- Create Role table
CREATE TABLE "Role" (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    Code NVARCHAR(100),
    Description NVARCHAR(100)
);

-- Create UserRole table
CREATE TABLE UserRole (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    UserId INT NOT NULL,
    RoleId INT NOT NULL,
    FOREIGN KEY (UserId) REFERENCES "User"(Id),
    FOREIGN KEY (RoleId) REFERENCES Role(Id)
);

-- Init data for Role table
INSERT INTO "Role" (Code, "Description")
VALUES ('Admin', 'Administrative user');

INSERT INTO "Role" (Code, "Description")
VALUES ('Normal', 'Normal user');

-- Init data for User table. HashedPassword for "Anhduc1711@"
-- Default admin user:     Admin/Anhduc1711@
INSERT INTO "User" (Username,"Password", Fullname, Email)
VALUES ('admin','beNNmT2+CaJsJlinACfCby5aiHIbPYMxzBxHmETlF4IZTMcd', 'Admin User', 'admin@identity.fpt.com');

-- Init data for UserRole table
INSERT INTO UserRole (UserId, RoleId)
VALUES (1, 1); 

