﻿using API.Controllers.DTOs;
using AutoMapper;
using Core.Interfaces.API;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("rest/roles")]
    [ApiController]
    [Authorize]
    public class RoleController : Controller
    {
        private readonly IRoleManager _roleManger;
        private readonly IMapper _mapper;

        public RoleController(IRoleManager roleManager, IMapper mapper)
        {
            _roleManger = roleManager;
            _mapper = mapper;
        }


        [HttpPost("add")]
        public async Task<IActionResult> CreateUser([FromBody] RoleDto roleDto)
        {
            var request = _mapper.Map<RoleDto, Role>(roleDto);

            await _roleManger.AddNewRole(request);

            return Ok();
        }
    }
}
