﻿using API.Controllers.DTOs;
using Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("rest/user-infos")]
    [ApiController]
    [Authorize]
    public class UserInfoController : ControllerBase
    {
        private readonly IUserInfoManager _userInfoManager;

        public UserInfoController(IUserInfoManager userInfoManager)
        {
            _userInfoManager = userInfoManager;
        }

        [HttpPost("fullname")]
        public async Task<IActionResult> UpdateUserFullName([FromBody] string fullName)
        {
            HttpContext context = HttpContext;
            string userId = "";

            if (context.User.Identity.IsAuthenticated)
            {
                userId = context.User.FindFirst("Id")?.Value;

                if (string.IsNullOrEmpty(userId))
                {
                    return BadRequest("Can not get id from token");
                }
            }

            if (fullName == null)
            {
                return BadRequest("Invalid request");
            }

            await _userInfoManager.UpdateUserFullName(int.Parse(userId), fullName);

            return Ok();
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] UpdatePasswordDto updatePasswordDto)
        {
            HttpContext context = HttpContext;
            string userId = "";

            if (context.User.Identity.IsAuthenticated)
            {
                userId = context.User.FindFirst("Id")?.Value;

                if (string.IsNullOrEmpty(userId))
                {
                    return BadRequest("Can not get id from token");
                }
            }

            if (updatePasswordDto.NewPassword != updatePasswordDto.ConfirmedPassword)
            {
                return BadRequest("Password does not match");
            }

            await _userInfoManager.ChangePassword(int.Parse(userId), updatePasswordDto.OldPassword, updatePasswordDto.NewPassword);

            return Ok();
        }
    }
}