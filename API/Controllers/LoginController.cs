﻿using API.Controllers.DTOs;
using Core.Interfaces.API;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// ONE NOTE: I am using Database first approach, so please Run Sql scritp that attached before running the application.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly IUserManager _userManager;
        private readonly ITokenService _tokenService;

        public LoginController(IUserManager userManager, ITokenService tokenService)
        {
            _userManager = userManager;
            _tokenService = tokenService;
        }

        /// <summary>
        /// FOR DEFAULT ADMIN USER ACCOUNT : Admin/Anhduc1711@
        /// </summary>
        /// <param name="loginDto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDto loginDto)
        {
            if (loginDto == null)
                return BadRequest();

            var user = await _userManager.GetUserLogin(loginDto.UserName, loginDto.Password);

            user.Token = await _tokenService.CreateJwt(user);
            var newAccessToken = user.Token;
            var newRefreshToken = await _tokenService.CreateRefreshToken(user);
            user.RefreshToken = newRefreshToken;
            user.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(5);
            await _userManager.UpdateUser(user);

            return Ok(new TokenApiDto()
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken
            });
        }

        /// <summary>
        /// Just for my test.
        /// </summary>
        /// <param name="tokenApiDto"></param>
        /// <returns></returns>
        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] TokenApiDto tokenApiDto)
        {
            if (tokenApiDto is null)
                return BadRequest("Invalid Client Request");

            string accessToken = tokenApiDto.AccessToken;
            string refreshToken = tokenApiDto.RefreshToken;
            var principal = await _tokenService.GetPrincipleFromExpiredToken(accessToken);
            var username = principal.Identity.Name;
            var user = await _userManager.GetUserByUserName(username);
            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                return BadRequest("Invalid Request");
            var newAccessToken = await _tokenService.CreateJwt(user);
            var newRefreshToken = await _tokenService.CreateRefreshToken(user);
            user.RefreshToken = newRefreshToken;

            _userManager.UpdateUser(user);

            return Ok(new TokenApiDto()
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken,
            });
        }
    }
}