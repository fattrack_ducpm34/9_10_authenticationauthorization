﻿using API.Controllers.DTOs;
using Core.Interfaces.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("rest/userroles")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserRoleController : ControllerBase
    {
        private readonly IUserRoleManager _userRoleManger;

        public UserRoleController(IUserRoleManager userRoleManger)
        {
            _userRoleManger = userRoleManger;
        }

        [HttpPost("add")]
        public async Task<IActionResult> CreateUser([FromBody] UserRoleDto userRoleDto)
        {
            await _userRoleManger.AddRoleForUser(userRoleDto.UserId, userRoleDto.RoleId);

            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<IActionResult> RemoveRoleOfUser([FromBody] UserRoleDto userRoleDto)
        {
            await _userRoleManger.RemoveRoleOfUser(userRoleDto.UserId, userRoleDto.RoleId);

            return Ok();
        }
    }
}