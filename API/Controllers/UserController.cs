﻿using API.Controllers.DTOs;
using AutoMapper;
using Core.Interfaces.API;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("rest/users")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;

        public UserController(IUserManager userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateUser([FromBody] UserDto userDto)
        {

            if (userDto.Password != userDto.ConfirmedPassword)
            {
                throw new ArgumentException("password and confirm password does not match!!!");
            }

            var request = _mapper.Map<UserDto, User>(userDto);

            await _userManager.AddUser(request);

            return Ok();
        }

        [HttpDelete("delete/{userId}")]
        public async Task<IActionResult> CreateUser([FromRoute] int userId)
        {
            await _userManager.DeleteUser(userId);

            return Ok();
        }
    }
}