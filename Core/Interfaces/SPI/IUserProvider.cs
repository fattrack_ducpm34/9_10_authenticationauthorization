﻿using Core.Models;

namespace Core.Interfaces.SPI
{
    public interface IUserProvider
    {
        public Task AddUser(User user);

        public Task UpdateUser(User user);

        public Task DeleteUser(int Id);

        public Task<User> GetUserLogin(string userName, string password);

        public Task<User> GetUserByUserName(string userName);
    }
}