﻿using Core.Models;

namespace Core.Interfaces.SPI
{
    public interface IRoleProvider
    {
        public Task AddNewRole(Role role);
    }
}