﻿namespace Core.Interfaces.SPI
{
    public interface IUserInfoProvider
    {
        public Task ChangePassword(int id, string oldPassword, string newPassword);

        public Task UpdateUserFullName(int id, string fullName);
    }
}