﻿using Core.Models;
using System.Security.Claims;

namespace Core.Interfaces.API
{
    public interface ITokenService
    {
        Task<string> CreateJwt(User user);

        public Task<string> CreateRefreshToken(User user);

        public Task<ClaimsPrincipal> GetPrincipleFromExpiredToken(string token);
    }
}