﻿using Core.Models;

namespace Core.Interfaces.API
{
    public interface IRoleManager
    {
        public Task AddNewRole(Role role);
    }
}