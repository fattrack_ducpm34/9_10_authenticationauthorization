﻿namespace Core.Interfaces.API
{
    public interface IUserInfoManager
    {
        public Task UpdateUserFullName(int id, string fullName);

        public Task ChangePassword(int id, string oldPassword, string newPassword);
    }
}