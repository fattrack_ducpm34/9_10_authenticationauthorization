﻿using Core.Interfaces.API;
using Core.Interfaces.SPI;
using Core.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Core.Services
{
    public class TokenService : ITokenService
    {
        private readonly IUserRoleProvider _userRoleProvider;

        public TokenService(IUserRoleProvider userRoleProvider)
        {
            _userRoleProvider = userRoleProvider;
        }

        public async Task<string> CreateJwt(User user)
        {
            List<UserRole> userRoles = await _userRoleProvider.GetUserRolesByUserId(user.Id);

            var jwtTokenHandler = new JwtSecurityTokenHandler();

            //TODO DUCPM34: It should move into appsettings.file
            var key = Encoding.ASCII.GetBytes("ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM");
            var identity = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim("Id", user.Id.ToString())
            });

            if (userRoles.Any())
            {
                foreach (var userRole in userRoles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, userRole.Role.Code));
                }
            }

            var credentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.Now.AddMinutes(5),
                SigningCredentials = credentials
            };
            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            return jwtTokenHandler.WriteToken(token);
        }

        public async Task<string> CreateRefreshToken(User user)
        {
            var tokenBytes = RandomNumberGenerator.GetBytes(64);
            var refreshToken = Convert.ToBase64String(tokenBytes);

            if (user.RefreshToken != null)
            {
                // Return the existing refresh token if it exists.
                return user.RefreshToken;
            }

            return refreshToken;
        }

        public async Task<ClaimsPrincipal> GetPrincipleFromExpiredToken(string token)
        {
            var key = Encoding.ASCII.GetBytes("ByYM000OLlMQG6VVVp1OH7Xzyr7gHuw1qvUC5dcGt3SNM");
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateLifetime = false
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("This is Invalid Token");
            return await Task.FromResult(principal);
        }
    }
}