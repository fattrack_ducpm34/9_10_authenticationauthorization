﻿using Core.Interfaces.API;
using Core.Interfaces.SPI;
using Core.Models;

namespace Core.Services
{
    public class RoleManager : IRoleManager
    {
        private readonly IRoleProvider _roleProvider;

        public RoleManager(IRoleProvider roleProvider)
        {
            _roleProvider = roleProvider;
        }

        public async Task AddNewRole(Role role)
        {
            await _roleProvider.AddNewRole(role);
        }
    }
}