﻿using Core.Interfaces.API;
using Core.Interfaces.SPI;
using Core.Models;

namespace Core.Services
{
    public class UserManager : IUserManager
    {
        private readonly IUserProvider _userProvider;

        public UserManager(IUserProvider userProvider)
        {
            _userProvider = userProvider;
        }

        public async Task AddUser(User user)
        {
            await _userProvider.AddUser(user);
        }

        public async Task DeleteUser(int id)
        {
            await _userProvider.DeleteUser(id);
        }

        public async Task<User> GetUserByUserName(string userName)
        {
            return await _userProvider.GetUserByUserName(userName);
        }

        public async Task<User> GetUserLogin(string userName, string password)
        {
            return await _userProvider.GetUserLogin(userName, password);
        }

        public async Task UpdateUser(User user)
        {
            await _userProvider.UpdateUser(user);
        }
    }
}