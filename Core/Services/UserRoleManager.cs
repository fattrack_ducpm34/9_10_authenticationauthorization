﻿using Core.Interfaces.API;
using Core.Interfaces.SPI;

namespace Core.Services
{
    public class UserRoleManager : IUserRoleManager
    {
        private readonly IUserRoleProvider _userRoleProvider;

        public UserRoleManager(IUserRoleProvider userRoleProvider)
        {
            _userRoleProvider = userRoleProvider;
        }

        public async Task AddRoleForUser(int userId, int roleId)
        {
            await _userRoleProvider.AddRoleForUser(userId, roleId);
        }

        public async Task RemoveRoleOfUser(int userId, int roleId)
        {
            await _userRoleProvider.RemoveRoleOfUser(userId, roleId);
        }
    }
}