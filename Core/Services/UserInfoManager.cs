﻿using Core.Interfaces.API;
using Core.Interfaces.SPI;

namespace Core.Services
{
    public class UserInfoManager : IUserInfoManager
    {
        private readonly IUserInfoProvider _userInfoProvider;

        public UserInfoManager(IUserInfoProvider userInfoProvider)
        {
            _userInfoProvider = userInfoProvider;
        }

        public async Task ChangePassword(int id, string oldPassword, string newPassword)
        {
            await _userInfoProvider.ChangePassword(id, oldPassword, newPassword);
        }

        public async Task UpdateUserFullName(int id, string fullName)
        {
            await _userInfoProvider.UpdateUserFullName(id, fullName);
        }
    }
}